﻿// SOBCPP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "List.h"
#include <cassert>

using namespace std;

void WczytajDane(fstream& plik, double zakresDolny, double zakresGorny)
{
	
	if (plik.good())
	{
		while (!plik.eof())
		{
			double liczba;
			char wejscie[100];
			plik >> wejscie;
			if (sscanf_s(wejscie, "%lf", &liczba) == 1)
			{
				cout << liczba<<" ";
			}
		}
	}
}



int main(int argc, char * argv[])
{
	int value;
	char what;
	string str;

	double zakresDolny = stringToValue<double>(argv[2]);
	double zakresGorny = stringToValue<double>(argv[3]);

	assert(zakresDolny < zakresGorny); //Zakres dolny musi być mniejszy od górnego
	cout << "Wczytane zostana liczby z przedzialu od " << zakresDolny << " do " << zakresGorny << endl;
	
	fstream plik;
	plik.open(argv[1], ios_base::in);
	assert(plik);
	WczytajDane(plik, zakresDolny, zakresGorny);
	
	

	cin >> value;
    return 0;
}

