#pragma once
#include <string>
#include <sstream>

struct el
{
	double v;
	el *next;
	el *previous;
	el();
	el(double v);
	virtual ~el();
};

template<class T>
T stringToValue(std::string str) {
	std::istringstream iss(str);
	T value;
	iss >> value;
	return value;
}

class List
{
	el *head;
	el *tail;
	el *addHead(double a);
	el *addTail(double a);
	el *removeHead();
	el *removeTail();
public:
	List();
	el *add(double a);
	void removeList();
	void showRightToLeft();
	void showLeftToRight();



	virtual ~List();
};

